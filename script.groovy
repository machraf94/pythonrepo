def buildapp(){
sh '''#!/bin/bash
    kill $(lsof -t -i:5000)
    pip3 install -r requirements.txt
    export DB=mongodb://127.0.0.1:27017/tasks
    nohup python3 run.py &
'''
}
return this